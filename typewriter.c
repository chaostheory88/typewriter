#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <signal.h>

#define BUF_SIZE 1024
#define READ_STDIN 1
#define READ_CONSOLE 2

/* Defines for ANSI colors */
#define RED		"\x1b[31m"
#define GREEN		"\x1b[32m"
#define YELLOW		"\x1b[33m"
#define BLUE		"\x1b[34m"
#define MAGENTA		"\x1b[35m"
#define CYAN		"\x1b[36m"
#define RESET		"\x1b[0m"

/* Defines for color number */
#define RED_NO		1
#define GREEN_NO	2
#define YELLOW_NO	3
#define	BLUE_NO		4
#define MAGENTA_NO	5
#define CYAN_NO		6
#define RANDOM		0x29a

/* Defines for bold, blink for now... */
#define BOLD	"\x1b[1m"
#define BLINK	"\x1b[5m"

extern int opterr;
opterr = 0;

static char buffer[BUF_SIZE];
static struct timespec ts;
static int color = 0;
static long speed = 1;
static char *effect = "none";

static void
error(const char *err)
{
  perror(err);
  exit(EXIT_FAILURE);
}


/* A signal handler to reset the program on CTRL-C */
static void
handler(int sig){
  printf(RESET "\n");	/* Yep this is not safe for now */
  exit(EXIT_SUCCESS);
}

/* Return a random color */
static int
getRandomColor(void)
{
  int randMax;
  int randMin;
  int random;
  
  randMax = rand() % CYAN_NO;
  randMin = rand() % RED_NO;
  random = (randMax - randMin) + 1;
  
  return random;
}

/* Print colored */
static void
printColored(int col,char c)
{ 
  switch(col){
    case RED_NO:
      printf(RED "%c"); break;
    case GREEN_NO:
      printf(GREEN "%c"); break;
    case YELLOW_NO:
      printf(YELLOW "%c"); break;
    case BLUE_NO:
      printf(BLUE "%c"); break;
    case MAGENTA_NO:
      printf(MAGENTA "%c"); break;
    case CYAN_NO:
      printf(CYAN "%c"); break;
    default:
      printf("%c");
  }
  fflush(stdout);
}

/* Performs typewriting operations getting chars from the buffer */
static int
typeWrite(char *buffer, int to)
{
  int i;
  int rndCol;
  ssize_t written;
  for(i=0; i<to; i++){
    if(color == RANDOM){
      rndCol = getRandomColor();
      printColored(rndCol,buffer[i]);
    }else{
      printColored(color,buffer[i]);
    }
    nanosleep(&ts,NULL);
  }
  return written;
}

/* Print a file in typewriter style to the console */
static int
printFile(char *filename)
{
  int fd,i;
  ssize_t reads;
  
  if((fd = open(filename,O_RDONLY)) == -1)
    return -1;
  else{
     while((reads = read(fd,buffer,BUF_SIZE)) > 0){
      typeWrite(buffer,reads);
     }
  }
  close(fd);
  return (reads > 0) ? reads : 0;
}

/* Print text given from console in typewriter style to the console */
static int
printConsole(char *arg)
{
  int i;
  
  strncpy(buffer,arg,BUF_SIZE-1);
  
  typeWrite(buffer,strlen(buffer));
}

/* Print text given from stdin */
static int
printStdIn(void)
{
  ssize_t reads;
  while((reads = read(0,buffer,1)) > 0)
    typeWrite(buffer,reads);
}

/* Display program usage and exit */
static void
usage(const char *pname)
{
	fprintf(stdout,"%s:\n\t[-f filename]\n\t[-i read from stdin]\n\t[-c console input]\n\t[-s speed {1=fast|9=very slow}]\n\t[-h show this help and exit]\n",pname);
	fprintf(stdout,"\t[-w color: 1=red, 2=green, 3=yellow, 4=blue, 5=magenta, 6=cyan, 666=random\n");
	fprintf(stdout,"\t[-e effect {bold|blink}]\n");
	fprintf(stdout,"written by Spike 2013.\n");
	exit(EXIT_FAILURE);
}

int
main(int argc, char *argv[])
{
	int opt, readFrom;
	char *input;
	struct sigaction sa;
	
	sa.sa_handler = handler;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	
	if(sigaction(SIGINT,&sa,NULL) == -1)
	  error("sigaction");
	
	if((argc > 1 && strcmp(argv[1],"-h") == 0) || argc == 1)
		usage(argv[0]);
	
	while((opt = getopt(argc,argv,"if:s:c:w:e:")) != -1){
	  switch(opt){
	    case 'f':
	      input = optarg;
	      break;
	    case 'c':
	      readFrom = READ_CONSOLE;
	      input = optarg;
	      break;
	    case 's':
	      speed = atol(optarg);
	      break;
	    case 'i':
	      readFrom = READ_STDIN;
	      break;
	    case 'w':
	      color = atoi(optarg);
	      break;
	    case 'e':
	      effect = optarg;
	      break;
	    default:
	      usage(argv[0]);
	  }
	}
	
	if(strcmp(effect,"bold") == 0)
	  printf(BOLD);
	else if(strcmp(effect,"blink") == 0)
	  printf(BLINK);
	
	ts.tv_sec = 0;
	ts.tv_nsec = (speed > 1 || speed <= 9) ? speed * 100000000 : 100000000;
	
	printf("\n");
	if(readFrom == READ_CONSOLE)
	  printConsole(input);
	else if(readFrom == READ_STDIN)
	  printStdIn();
	else
	  printFile(input);
	printf(RESET "\n\n");

	exit(EXIT_SUCCESS);

}
